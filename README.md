
## Synvata interview.
Thank you for interviewing at Synvata! We appreciate you taking the time to complete this exercise. 

## Getting Started

For taking this challenge you will have to set up a React project. We are not testing your ability to configure webpack, you might as well use create-react-app.   
The only dependency required is React at version 16.8 or higher, and **you are required to code using React hooks.**   
**Note 1: Dot not worry too much about error handling or validations. The goal here is to assess your component tree.**   
**Note 2: Styling/CSS is not required at all. In fact, we recommend you to not do any styling so you can save time and focus on what is being evaluated.**   

## The problem

You will be building an interactive page (like a spreadsheet) with two tabs, that loads data from an API. All formulas (**click on each cells with values to see them**) can be found on an actual spreadsheet through this link (https://docs.google.com/spreadsheets/d/1Bnv5BX8h_AXViyPOCkfyXP9pEhb6578Rg7nTokg_oS4/edit?usp=sharing) and further instructions are found down below.

 - Items circled in pink: Fields with a gray background and blue text are inputable fields (text fields). As soon as the user leaves the text field, you should apply the proper formatting (whether that’s currency (e.g.: $1,000,00) or percentage (e.g.: %0.20)). The default value should be 0.
 - Light blue arrow: As you will on the spreadsheet, "CECL For unfunded" number is a reference from the unfunded commitments tab.
 - Items circled in light green: They are text inputs, and allow a number from 1 to 5. Upon changing this value you should call an API informing the row identifier and the number for years. The default value is 5. API instructions can be found in the next section. 
 - Items circled in blue: That’s data retrieved from the API. Like mentioned above upon changing the number of years you will re-load data for given row.
 - Item circled in yellow: Its default value comes from the “Summary Tab”. The user is allowed to override that value, but if any of its dependency changes on the “Summary Tab” the field’s value should be reset to the default one.


![Summary Tab instructions](SummaryTab.png)
![Unfunded Tab instructions](UnfundedTab.png)


## API Instructions
URL: https://react-code-challenge.herokuapp.com   

URI and parameters:
`https://react-code-challenge.herokuapp.com/codeChallenge?fieldId=1&years=5`

JSON response sample:

    {
      "averageLoans": 53054991,
      "netChargeoffsSum": 1481272
    }

Available fields:

 - Unsecured Credit Card Loans > Field Id 1 
 - New Vehicles Loans > Field Id 2

Examples: 
For row 1 you should call:   
`/codeChallenge?fieldId=1&years=5`   
For row 2 you should call:  
`/codeChallenge?fieldId=2&years=5`   


# Have a good time!